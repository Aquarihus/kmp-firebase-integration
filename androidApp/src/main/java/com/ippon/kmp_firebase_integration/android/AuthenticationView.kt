package com.ippon.kmp_firebase_integration.android

import android.app.Activity
import android.util.Log
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.ippon.kmp_firebase_integration.viewmodel.AuthenticationViewModel


@Composable
fun AuthenticationView(
    authenticationViewModel: AuthenticationViewModel
) {
    val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken("86184358628-u14g08iq6mnje0qo76ke6tg0l69k75k4.apps.googleusercontent.com")
        .requestEmail()
        .build()

    val activity = LocalContext.current as Activity
    val googleSignInClient = GoogleSignIn.getClient(activity, gso)

    val launcher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                try {
                    val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    authenticationViewModel.signInWithGoogle(account.idToken.toString(), null)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.w("TAG", "Google sign in failed", e)
                }
        }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Button(
            onClick = { authenticationViewModel.signInAnonymously() },
            modifier = Modifier
                .padding(12.dp),
            shape = RoundedCornerShape(12.dp),
        ) {
            Text(text = "Continuer en invité")
        }
        GoogleSignInButton(onClick = {
            val signInIntent = googleSignInClient.signInIntent
            launcher.launch(signInIntent)
        })
    }
}

@Composable
fun GoogleSignInButton(
    onClick: () -> Unit
) {
    Button(
        onClick = onClick,
        shape = RoundedCornerShape(12.dp),
        border = BorderStroke(1.dp, Color.LightGray),
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.White,
        ),
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_google_logo),
            contentDescription = "Logo Google"
        )
        GoogleSignInText()
    }
}

@Composable
fun GoogleSignInText() {
    Text(
        text = "Continuer avec Google",
        color = Color.Gray,
        fontSize = 14.sp,
        modifier = Modifier.padding(4.dp),
    )
}

@Preview
@Composable
fun AuthenticationViewPreview() {
    AuthenticationView(AuthenticationViewModel())
}