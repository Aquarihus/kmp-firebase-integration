# KMP Firebase Integration


## Presentation

KMP Firebase Integration is a sample project made to illustrate how we can use the [Firebase Kotlin SDK](https://firebaseopensource.com/projects/gitliveapp/firebase-kotlin-sdk/) library to authenticate user in a [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) application. The main purpose is to write an article about this integration. You can find the article [here]() (TODO)

KMP Firebase Integration is made with the following technologies :
- [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) to share business code.
- [Jetpack Compose](https://developer.android.com/jetpack/compose?authuser=1) as a UI framework for Android.
- [SwiftUI](https://developer.apple.com/xcode/swiftui/) as a UI framework for iOS.
- The library [Firebase Kotlin SDK](https://firebaseopensource.com/projects/gitliveapp/firebase-kotlin-sdk/).
- [Firebase](https://console.firebase.google.com/u/0/?pli=1) for the authentication.


## Features

- Use the Firebase Kotlin SDK library in the shared code
- Authenticate a user with an anonymous account
- Get Google tokens in native Android and iOS
- Sign in with Google

## Installation

1. Clone or download the source code from this page.
2. Download [Android Studio](https://developer.android.com/studio).
3. Open the project on [Android Studio](https://developer.android.com/studio).

If you want to edit the code or run the application on iOS:

4. Download [XCode](https://developer.apple.com/xcode/).
5. Open the **iosApp** folder in [XCode](https://developer.apple.com/xcode/).

## Versions
KMP Firabse Integration was created using the following tools:
- [Android Studio Giraffe](https://developer.android.com/studio) | 2022.3.1 Patch 4
- [XCode](https://developer.apple.com/xcode/) - 14.0.1
- [Gradle](https://gradle.org/) version 8.0
- [Kotlin Mulitplatform Mobile](https://plugins.jetbrains.com/plugin/14936-kotlin-multiplatform-mobile/versions) plugin 0.8.1(223)-26 

