//
//  AppDelegate.swift
//  iosApp
//
//  Created by ippon on 14/12/2023.
//  Copyright © 2023 orgName. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions:
                     [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        FirebaseApp.configure()
        
        return true
    }
}
