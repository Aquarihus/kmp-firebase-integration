import SwiftUI
import shared
import GoogleSignIn


struct ContentView: View {
    var authenticationViewModel: AuthenticationViewModel = AuthenticationViewModel()
    
	var body: some View {
        VStack(alignment: .center) {
            Button(action: {
                  authenticationViewModel.signInAnonymously()
            }) {
                Text("Continuer en invité")
            }
            .onOpenURL { url in
                      GIDSignIn.sharedInstance.handle(url)
                    }
            CustomGoogleButton(action: {
                let presentingViewController = (UIApplication.shared.connectedScenes.first as? UIWindowScene)?.windows.first?.rootViewController
                handleSignInButton(presentingViewController: presentingViewController)
            })
            
        }
	}
    
    func handleSignInButton(presentingViewController: UIViewController?) {
        if (presentingViewController == nil) {
            return
        }

        GIDSignIn.sharedInstance.signIn(withPresenting: presentingViewController!) { signInResult, error in
            guard error == nil else { return }
            guard let signInResult = signInResult else { return }

            signInResult.user.refreshTokensIfNeeded { user, error in
                guard error == nil else { return }
                guard let user = user else { return }

                let idToken = user.idToken
                let accessToken = user.accessToken
                
                authenticationViewModel.signInWithGoogle(idToken: idToken?.tokenString, accessToken: accessToken.tokenString)
            }
        }
    }
}


struct CustomGoogleButton: View {
            
    var action: (() -> Void)?

    var body: some View {
        ZStack(){
            Button{
                self.action?()
            }label: {
                HStack {
                    Image("IcGoogleLogo")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 20, height: 20)
                        .scaledToFit()
                        .clipped()
                    Text("Continuer avec Google")
                    .padding()
                }.frame(height: 20)
                    .padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(.gray, lineWidth: 1)
                    )
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
