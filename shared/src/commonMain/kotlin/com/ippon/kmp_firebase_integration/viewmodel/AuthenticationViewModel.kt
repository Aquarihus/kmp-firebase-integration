package com.ippon.kmp_firebase_integration.viewmodel

import com.ippon.kmp_firebase_integration.service.AuthenticationInterface
import com.ippon.kmp_firebase_integration.service.AuthenticationService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AuthenticationViewModel constructor(
    private val authenticationService: AuthenticationInterface = AuthenticationService(),
) {
    constructor() : this(AuthenticationService())

    private val coroutineScope: CoroutineScope = MainScope()

    fun signInAnonymously() {
        coroutineScope.launch {
            authenticationService.signInAnonymously()
        }
    }

    fun signInWithGoogle(idToken: String?, accessToken: String?) {
        coroutineScope.launch {
            authenticationService.signInWithGoogle(idToken = idToken, accessToken = accessToken)
        }
    }
}