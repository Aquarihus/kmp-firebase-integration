package com.ippon.kmp_firebase_integration.service

interface AuthenticationInterface {
    suspend fun signInAnonymously()
    suspend fun signInWithGoogle(idToken: String?, accessToken: String?)
}