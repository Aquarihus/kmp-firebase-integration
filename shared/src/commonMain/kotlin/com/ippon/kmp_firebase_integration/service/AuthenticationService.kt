package com.ippon.kmp_firebase_integration.service

import dev.gitlive.firebase.Firebase
import dev.gitlive.firebase.auth.FirebaseAuth
import dev.gitlive.firebase.auth.GoogleAuthProvider
import dev.gitlive.firebase.auth.auth

class AuthenticationService(
    private val firebaseAuth: FirebaseAuth = Firebase.auth
) : AuthenticationInterface {
    override suspend fun signInAnonymously() {
        try {
            firebaseAuth.signInAnonymously()
        } catch (e: Exception) {
            throw e
        }
    }

    override suspend fun signInWithGoogle(idToken: String?, accessToken: String?) {
        val credential = GoogleAuthProvider.credential(idToken, accessToken)
        firebaseAuth.signInWithCredential(credential)
    }
}